The code execution for Storage Manager starts with building the code with Make file which is responsible for creating the test_assign1 Binary file and linking test_assign1_1.c file with all corresponding *.h and *.c files. Make file also inter-links each of the files in the directory with each other.



Procedure to Execute the storage manager:-

1)Copy all the files to one folder
2)In Unix Terminal navigate to folder where files are stored.
3)Execute "make" command
4)Execute "./test_assign1_1" command


Structure of Storage Manager



1) dberror.c -> It contains printerror and errormessage function. For the specific error it outputs the error code and specific error message.



2)dberror.h -> It defines page_size constant as 4096 and definition for RC(return codes) for various File and Block functions in storage_mgr. 



3)storage_mgr.c -> This is the file responsible for containing various file and block functions which are called from test_assign1_1.c It contains read and write function from file on disk to buffer and vise-versa. It contains creating, opening and closing file functions as well. It implements dberror.h and test_helpers.h file.



Functionality of each functions :-
 1.createPageFile (char *fileName) :- Creates a new file with passed fileName and also initialize its contents with '\0' upto PAGE_SIZE which is 4096bytes.

 2. openPageFile (char *fileName, SM_FileHandle *fHandle):- Opens an existing file with fileName and if not found returns  an RC_FILE_NOT_FOUND  error. After opening a file it initialize all the file related parameters in fHandle.

 3. closePageFile (SM_FileHandle *fHandle):- It closes the open file and releases all the consumed resources by file  operations.

 4.destroyPageFile (char *fileName):- It removes file from disk system

 5.readBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage):- It reads a Block of data from file specified    by pageNum to memory pointed by memPage pointer. if desired page not found then it returns RC_READ_NON_EXISTING_PAGE error.

 6.getBlockPos (SM_FileHandle *fHandle):- It returns current page position of File.
 
 7.readFirstBlock(SM_FileHandle *fHandle, SM_PageHandle memPage):- It reads the First Page Block of file into memory  pointed by memPage. if desired page not found then it returns RC_READ_NON_EXISTING_PAGE error.

 8.readPreviousBlock (SM_FileHandle *fHandle, SM_PageHandle memPage):- It reads the block page previous to the block  pointed by current page pointer of file into memory which is pointed by memPage.if desired page not found then it returns RC_READ_NON_EXISTING_PAGE error.

9. readCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage):- It reads current Page Block of file indicated by  current page pointer into memory which is pointed by memPage. if desired page not found then it returns RC_READ_NON_EXISTING_PAGE error.

 10.readNextBlock (SM_FileHandle *fHandle, SM_PageHandle memPage):- It reads the block page next to the block  pointed by  current page pointer of file into memory which is pointed by memPage. if desired page not found then it returns RC_READ_NON_EXISTING_PAGE error.

 11.readLastBlock (SM_FileHandle *fHandle, SM_PageHandle memPage):- It reads last page block of file into memory which is pointed by memPage. if desired page not found then it returns RC_READ_NON_EXISTING_PAGE error.

 12. writeBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage):- It writes a block of data read from meory pointed by memPage to file page indicated by pageNum. If write operation fails then it returns RC_WRITE_FAILED error.

 13.writeCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage):- It writes a block of data from memory pointed by pageNum to the page on file which is pointed by Current page pointer of file.If write operation fails then it returns RC_WRITE_FAILED error.

 14. appendEmptyBlock (SM_FileHandle *fHandle):- It appends an empty page block to the end of the file.or.

 15. ensureCapacity (int numberOfPages, SM_FileHandle *fHandle):- It checks whether file has numberOfPages or not. if not then it creates new empty pages in a file upto numberOfPages.



4)storage_mgr.h -> It contains definition for all functions and interfaces that storage_mgr.c file calls. It contains SM_FileHandle and SM_PageHandle structure definition used for hadling file and page parameters. Definitions to create, open and close file as well as create, write, read and append block are defined in this file.



5)test_assign1_1.c -> It is the main file containing all function calls which invokes storage_mgr.c, dberror.c, test_helper.c files on execution. It invokes two main functions testCreateOpenClose() and testSinglePageContent() which inturn calls various functions within storage_mgr.c, dberror.c, test_helper.c files.

 1. testCreateOpenClose():- It checks for creation and opening functionalities of file.

 2.testSinglePageContent():- It test for creation and opening of file and then reads first block of data from file into  memory. after reading into memory it checks the content of memory to cross verify the read opertion.
then it writes a block of data from memory to file at position specified pageNum. then it again reads the data from file to cross verify the read opertion. then it destroyes the file.

 3.testReadWriteAppendBlocks():- after creation and opening of file, it writes a current page block of file from memory . and then it cross verifies the written block by reading current block from file. 
then it appends an empty block to a file and reads test read last block operation. it then reads previous block from file two times. On second time it should give an error which was expected as there are only two pages and storage manager attemps to read page which doesn't exist. It then performs two times read next operation and on second time it should written an error which is an expected error as storage manager reads page greater than the total number of pages in file. It then calls ensure capacity method by passing numberOfPages=5, which will create additional pages in a file. in the end it calls destroy method which will remove the file from file system.  




6)test_helper.h -> The file contains validation tasks for each function that are called within test_assign1. It is responsible for printing the RC code and RC message passed based on function's success or failure depnding on the values being passed to it.



7)MakeFile -> It is responsible for making an executable file for test_assign1_1.c which is the central calling file for all Storage Manager functions and it inturn calls each and every *.c and *.h files in the Folder.



General Flow of Storage Manager


Make command is responsible for creaing a binary executable file for test_assign1_1.c. The file test_assign1_1.c calls testCreateOpenClose() and testSinglePageContent() function which is responsible in calling various File and Blocks functions in storage_mgr.c. As per the exection outcome of discrete function disparate RC Code and RC messages will be displayed by the interface. On successful cmpletion of calling FUnction RC_OK is displayed. Otherwise, any other error message gets displayed.
